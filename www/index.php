<?php

$home_team = 'Leicester Tigers';
$default_opponent = 'Harlequins';
$opposition = '';
$calendar_url = 'http://e-qo.de/1fm.ics';
$sites = array(
	'Bath Rugby'	=> 'http://www.bathrugby.com/',
	'Benetton Treviso'	=> 'http://www.benettonrugby.it/',
	'Exeter Chiefs'	=> 'http://www.exeterchiefs.co.uk/',
	'Gloucester Rugby'	=> 'http://www.gloucesterrugby.co.uk/',
	'Gloucester United'	=> 'http://www.gloucesterrugby.co.uk/',
	'Harlequins'	=> 'http://www.quins.co.uk/',
	'London Irish'	=> 'http://www.london-irish.com/',
	'London Wasps'	=> 'http://www.wasps.co.uk/',
	'London Welsh'	=> 'http://www.london-welsh.co.uk/',
	'Newcastle Falcons'	=> 'http://www.newcastlefalcons.co.uk/',
	'Northampton Saints'	=> 'http://www.northamptonsaints.co.uk/',
	'Northampton Wanderers'	=> 'http://www.northamptonsaints.co.uk/',
	'Nottingham Rugby'	=> 'http://www.nottinghamrugby.co.uk/',
	'Ospreys'	=> 'http://www.ospreysrugby.com/',
	'Sale Jets'	=> 'http://www.salefc.com/',
	'Sale Sharks'	=> 'http://www.salefc.com/',
	'Saracens'	=> 'http://www.saracens.com/',
	'Toulouse'	=> 'http://www.stadetoulousain.fr/',
	'Worcester Warriors'	=> 'http://www.warriors.co.uk/',
	'Worcester Cavaliers'	=> 'http://www.warriors.co.uk/',
);

require 'vendor/iCalcreator/iCalcreator.class.php';
$config = array( "unique_id" => "example.com" );
$v = new vcalendar( $config );
$v->setConfig('url', $calendar_url);
$v->parse();
$v->sort();

while ($event = $v->getComponent('vevent'))
{
	/* @var $event vevent */
	$event_timestamp = mktime(0, 0, 0, $event->dtstart['value']['month'], $event->dtstart['value']['day'], $event->dtstart['value']['year']);
	if ($event_timestamp > time())
	{
		break;
	}
}

if ($event)
{
	preg_match('/^(.+) vs? (.+?)(?: \(.* Cup\))?$/', $event->summary['value'], $teams);
	if ($teams)
	{
		$opposition = trim(($teams[1] != $home_team) ? $teams[1] : $teams[2]);
	}
}

if (!$opposition || !isset($sites[$opposition]))
{
	$opposition = $default_opponent;
}

$url = $sites[$opposition];

?>
<html>
	<head>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8'>
		<meta name="KEYWORDS" content="Txxx">
		<title>supportleicester.com</title>
	</head>
	<frameset rows='100%, *' frameborder=no framespacing=0 border=0>
		<frame src="<?= $url ?>" name=mainwindow frameborder=no framespacing=0 marginheight=0 marginwidth=0></frame>
	</frameset>
	<noframes>
		<h2>Your browser does not support frames. We recommend upgrading your browser.</h2>
		<br><br>
		<center>Click <a href="<?= $url ?>" >here</a> to enter the site.</center>
	</noframes>
</html>
